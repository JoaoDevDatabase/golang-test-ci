package main

import "fmt"

type Name struct {
	Nome  string
	Idade int
}

func (n Name) exibir() {
	fmt.Println("O seu nome e: ", n.Nome)
	fmt.Println("E voce tem ", n.Idade, " anos, Uau!! ")
}
func main() {
	mensagem := Name{
		Nome:  "João",
		Idade: 18,
	}
	mensagem.exibir()
}
